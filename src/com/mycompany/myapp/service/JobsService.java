package com.mycompany.myapp.service;

import java.util.ArrayList;
import java.util.List;

import com.mycompany.myapp.dao.JobsDAO;
import com.mycompany.myapp.domain.JobsDTO;
import com.mycompany.myapp.domain.PagginatedDTO;
import com.mycompany.myapp.entity.Jobs;

public class JobsService
{
	private JobsDAO jobsDAO;
	
	public JobsService()
	{
		this.jobsDAO = new JobsDAO();
	}
	
	public JobsDTO findById(String id)
	{	
		Jobs jobs = jobsDAO.findById(id);
		JobsDTO jobDTO = new JobsDTO(jobs.getJobId(), jobs.getJobTitle(), jobs.getMinSalary(), jobs.getMaxSalary());
		return jobDTO;
	}
	
	public List<JobsDTO> findAll() 
	{
		List<JobsDTO> jobsDTOList = new ArrayList<JobsDTO>();		
		List<Jobs> jobsList = jobsDAO.findAll();
		
		for(Jobs job : jobsList)
		{
			JobsDTO jobDTO = new JobsDTO(job.getJobId(), job.getJobTitle(), job.getMinSalary(), job.getMaxSalary());
			jobsDTOList.add(jobDTO);			
		}		
		return jobsDTOList;
	}
	
	public void persist(JobsDTO jobDTO) 
	{	
		Jobs job = new Jobs();
		job.setJobId(jobDTO.getJobId());
		job.setJobTitle(jobDTO.getJobTitle());
		job.setMinSalary(jobDTO.getMinSalary());
		job.setMaxSalary(jobDTO.getMaxSalary());
		jobsDAO.persist(job);
	}
	
	public void update(String jobId, JobsDTO jobDTO) 
	{	
		Jobs job = jobsDAO.findById(jobId);	
		job.setJobTitle(jobDTO.getJobTitle());
		job.setMinSalary(jobDTO.getMinSalary());
		job.setMaxSalary(jobDTO.getMaxSalary());
		jobsDAO.update(job);
	}
	
	public PagginatedDTO<JobsDTO> findAllPaginated(int pageNum, int pageSize) 
	{	
		PagginatedDTO<JobsDTO> jobsDTOPage = new PagginatedDTO<JobsDTO>();
		PagginatedDTO<Jobs> jobsPage = jobsDAO.findAllPaginated(pageNum, pageSize);
		List<JobsDTO> jobsDTOList = new ArrayList<JobsDTO>();
		List<Jobs> jobsList = jobsPage.getResults();
		
		for(Jobs job : jobsList)
		{
			JobsDTO jobDTO = new JobsDTO(job.getJobId(), job.getJobTitle(), job.getMinSalary(), job.getMaxSalary());
			
			jobsDTOList.add(jobDTO);			
		}
		
		jobsDTOPage.setResults(jobsDTOList);
		jobsDTOPage.setNumber(jobsPage.getNumber());
		jobsDTOPage.setNumberOfElements(jobsPage.getNumberOfElements());
		jobsDTOPage.setTotalElements(jobsPage.getTotalElements());
		jobsDTOPage.setTotalPages(jobsPage.getTotalPages());
		
		return jobsDTOPage;
	}
	
	
}
