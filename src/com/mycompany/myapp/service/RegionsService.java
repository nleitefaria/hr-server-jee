package com.mycompany.myapp.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.mycompany.myapp.dao.RegionsDAO;
import com.mycompany.myapp.domain.RegionsDTO;
import com.mycompany.myapp.entity.Regions;

public class RegionsService {
	
	private RegionsDAO regionsDAO;
	
	public RegionsService()
	{
		this.regionsDAO = new RegionsDAO();
	}
	
	public RegionsDTO findById(BigDecimal id)
	{	
		Regions region = regionsDAO.findById(id);
		RegionsDTO regionDTO = new RegionsDTO(region.getRegionId(), region.getRegionName());
		return regionDTO;
	}
	
	public List<RegionsDTO> findAll() 
	{
		List<RegionsDTO> regionDTOList = new ArrayList<RegionsDTO>();		
		List<Regions> regionList = regionsDAO.findAll();
		
		for(Regions region : regionList)
		{
			RegionsDTO regionDTO = new RegionsDTO(region.getRegionId(), region.getRegionName());
			regionDTOList.add(regionDTO);			
		}		
		return regionDTOList;
	}
	
	public void persist(RegionsDTO regionsDTO) 
	{	
		Regions region = new Regions(regionsDTO.getRegionId(), regionsDTO.getRegionName(), null);	
		regionsDAO.persist(region);
	}
	
	public void update(BigDecimal regionId, RegionsDTO regionsDTO) 
	{	
		Regions region = regionsDAO.findById(regionId);
		region.setRegionName(regionsDTO.getRegionName());
		regionsDAO.update(region);
	}

}
