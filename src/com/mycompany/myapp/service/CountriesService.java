package com.mycompany.myapp.service;

import java.util.ArrayList;
import java.util.List;

import com.mycompany.myapp.dao.CountriesDAO;
import com.mycompany.myapp.dao.RegionsDAO;
import com.mycompany.myapp.domain.CountriesDTO;
import com.mycompany.myapp.domain.PagginatedDTO;
import com.mycompany.myapp.domain.RegionsDTO;
import com.mycompany.myapp.entity.Countries;
import com.mycompany.myapp.entity.Regions;

public class CountriesService 
{
	private CountriesDAO countriesDAO;
	private RegionsDAO regionsDAO;
	
	public CountriesService()
	{
		this.countriesDAO = new CountriesDAO();
		this.regionsDAO = new RegionsDAO();
	}
	
	public CountriesDTO findById(String id)
	{	
		Countries country = countriesDAO.findById(id);
		RegionsDTO regionDTO = new RegionsDTO(country.getRegions().getRegionId(), country.getRegions().getRegionName());	
		CountriesDTO countriesDTO = new CountriesDTO(country.getCountryId(), regionDTO, country.getCountryName());
		return countriesDTO;
	}
	
	public List<CountriesDTO> findAll() 
	{
		List<CountriesDTO> countriesDTOList = new ArrayList<CountriesDTO>();		
		List<Countries> countriesList = countriesDAO.findAll();
		
		for(Countries country : countriesList)
		{
			RegionsDTO regionDTO = new RegionsDTO(country.getRegions().getRegionId(), country.getRegions().getRegionName());	
			CountriesDTO countriesDTO = new CountriesDTO(country.getCountryId(), regionDTO, country.getCountryName());
			countriesDTOList.add(countriesDTO);			
		}		
		return countriesDTOList;
	}
	
	public PagginatedDTO<CountriesDTO> findAllPaginated(int pageNum, int pageSize) 
	{	
		PagginatedDTO<CountriesDTO> countriesDTOPage = new PagginatedDTO<CountriesDTO>();
		PagginatedDTO<Countries> countriesPage = countriesDAO.findAllPaginated(pageNum, pageSize);
		List<CountriesDTO> countriesDTOList = new ArrayList<CountriesDTO>();
		List<Countries> countriesList = countriesPage.getResults();
		
		for(Countries country : countriesList)
		{
			RegionsDTO regionDTO = new RegionsDTO(country.getRegions().getRegionId(), country.getRegions().getRegionName());	
			CountriesDTO countriesDTO = new CountriesDTO(country.getCountryId(), regionDTO, country.getCountryName());
			countriesDTOList.add(countriesDTO);			
		}
		
		countriesDTOPage.setResults(countriesDTOList);
		countriesDTOPage.setNumber(countriesPage.getNumber());
		countriesDTOPage.setNumberOfElements(countriesPage.getNumberOfElements());
		countriesDTOPage.setTotalElements(countriesPage.getTotalElements());
		countriesDTOPage.setTotalPages(countriesPage.getTotalPages());
		
		return countriesDTOPage;
	}
	
	public void persist(CountriesDTO countriesDTO) 
	{	
		Regions region = regionsDAO.findById(countriesDTO.getRegions().getRegionId());		
		
		if(region == null)
		{
			region = new Regions(countriesDTO.getRegions().getRegionId(), countriesDTO.getRegions().getRegionName(), null);
			regionsDAO.persist(region);
		}
		
		Countries country = new Countries(countriesDTO.getCountryId(), region, countriesDTO.getCountryName(), null);
		countriesDAO.persist(country);
	}
	
	public void update(String countryId, CountriesDTO countriesDTO) 
	{	
		Regions region = regionsDAO.findById(countriesDTO.getRegions().getRegionId());	
		Countries country = countriesDAO.findById(countryId);
		country.setCountryName(countriesDTO.getCountryName());
		country.setRegions(region);	
		countriesDAO.update(country);
	}
	
}
