package com.mycompany.myapp.service;

import java.util.ArrayList;
import java.util.List;

import com.mycompany.myapp.dao.DepartmentsDAO;
import com.mycompany.myapp.dao.LocationsDAO;
import com.mycompany.myapp.domain.CountriesDTO;
import com.mycompany.myapp.domain.DepartmentsDTO;
import com.mycompany.myapp.domain.LocationsDTO;
import com.mycompany.myapp.domain.RegionsDTO;
import com.mycompany.myapp.entity.Countries;
import com.mycompany.myapp.entity.Departments;
import com.mycompany.myapp.entity.Locations;
import com.mycompany.myapp.entity.Regions;

public class DepartmentsService {
	
	private DepartmentsDAO departmentsDAO;
	private LocationsDAO locationsDAO;
	
	public DepartmentsService()
	{
		this.departmentsDAO = new DepartmentsDAO();
		this.locationsDAO = new LocationsDAO();
	}
	
	public DepartmentsDTO findById(short id)
	{	
		Departments departement = departmentsDAO.findById(id);		
		Locations locations = departement.getLocations();		
		Countries country = locations.getCountries();	
		Regions region = country.getRegions();		
		RegionsDTO regionDTO = new RegionsDTO(region.getRegionId(), region.getRegionName());		
		CountriesDTO countriesDTO = new CountriesDTO(country.getCountryId(), regionDTO, country.getCountryName());		
		LocationsDTO locationsDTO = new LocationsDTO(locations.getLocationId(), countriesDTO ,locations.getStreetAddress(), locations.getPostalCode(), locations.getCity(), locations.getStateProvince());
	 
		DepartmentsDTO departementDTO = new DepartmentsDTO(departement.getDepartmentId(), departement.getDepartmentName(), locationsDTO);
		return departementDTO;
	}
	
	public List<DepartmentsDTO> findAll() 
	{
		List<DepartmentsDTO> departmentsDTOList = new ArrayList<DepartmentsDTO>();		
		List<Departments> departmentsList = departmentsDAO.findAll();
		
		for(Departments departement : departmentsList)
		{
			Locations locations = departement.getLocations();		
			Countries country = locations.getCountries();	
			Regions region = country.getRegions();		
			RegionsDTO regionDTO = new RegionsDTO(region.getRegionId(), region.getRegionName());		
			CountriesDTO countriesDTO = new CountriesDTO(country.getCountryId(), regionDTO, country.getCountryName());		
			LocationsDTO locationsDTO = new LocationsDTO(locations.getLocationId(), countriesDTO ,locations.getStreetAddress(), locations.getPostalCode(), locations.getCity(), locations.getStateProvince());
			DepartmentsDTO departementDTO = new DepartmentsDTO(departement.getDepartmentId(), departement.getDepartmentName(), locationsDTO);
			departmentsDTOList.add(departementDTO);			
		}		
		return departmentsDTOList;
	}
	
	public void persist(DepartmentsDTO departmentsDTO) 
	{
		Locations location = locationsDAO.findById(departmentsDTO.getLocations().getLocationId());
		Departments department = new Departments(departmentsDTO.getDepartmentId(), location, null, departmentsDTO.getDepartmentName(), null, null);
		departmentsDAO.persist(department);
	}
	
	public void update(short countryId, DepartmentsDTO departmentsDTO) 
	{	
		Locations location = locationsDAO.findById(departmentsDTO.getLocations().getLocationId());
		Departments department = new Departments();
		department.setDepartmentName(departmentsDTO.getDepartmentName());
		department.setLocations(location);
		departmentsDAO.update(department);
		
	}

}
