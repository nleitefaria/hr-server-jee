package com.mycompany.myapp.service;

import java.util.ArrayList;
import java.util.List;

import com.mycompany.myapp.dao.CountriesDAO;
import com.mycompany.myapp.dao.LocationsDAO;
import com.mycompany.myapp.dao.RegionsDAO;
import com.mycompany.myapp.domain.CountriesDTO;
import com.mycompany.myapp.domain.LocationsDTO;
import com.mycompany.myapp.domain.RegionsDTO;
import com.mycompany.myapp.entity.Countries;
import com.mycompany.myapp.entity.Locations;
import com.mycompany.myapp.entity.Regions;

public class LocationsService 
{	
	private LocationsDAO locationsDAO;
	private CountriesDAO countriesDAO;
	private RegionsDAO regionsDAO;
	
	public LocationsService()
	{
		this.locationsDAO = new LocationsDAO();
		this.countriesDAO = new CountriesDAO();
		this.regionsDAO = new RegionsDAO();
	}
	
	
	public LocationsDTO findById(Short id)
	{	
		Locations locations = locationsDAO.findById(id);
		
		Countries country = locations.getCountries();	
		Regions region = country.getRegions();		
		RegionsDTO regionDTO = new RegionsDTO(region.getRegionId(), region.getRegionName());		
		CountriesDTO countriesDTO = new CountriesDTO(country.getCountryId(), regionDTO, country.getCountryName());
		
		LocationsDTO locationsDTO = new LocationsDTO(locations.getLocationId(), countriesDTO ,locations.getStreetAddress(), locations.getPostalCode(), locations.getCity(), locations.getStateProvince());
	    return locationsDTO;
	}
	
	public List<LocationsDTO> findAll() 
	{
		List<LocationsDTO> locationDTOList = new ArrayList<LocationsDTO>();		
		List<Locations> locationList = locationsDAO.findAll();
		
		for(Locations locations : locationList)
		{
			Countries country = locations.getCountries();	
			Regions region = country.getRegions();		
			RegionsDTO regionDTO = new RegionsDTO(region.getRegionId(), region.getRegionName());		
			CountriesDTO countriesDTO = new CountriesDTO(country.getCountryId(), regionDTO, country.getCountryName());
			
			LocationsDTO locationsDTO = new LocationsDTO(locations.getLocationId(), countriesDTO ,locations.getStreetAddress(), locations.getPostalCode(), locations.getCity(), locations.getStateProvince());
		    locationDTOList.add(locationsDTO);			
		}		
		return locationDTOList;
	}
	
	public void persist(LocationsDTO locationDTO) 
	{
		Countries country = countriesDAO.findById(locationDTO.getCountriesDTO().getCountryId());
		
		if(country == null)
		{
			Regions region = regionsDAO.findById(locationDTO.getCountriesDTO().getRegions().getRegionId());	
			country = new Countries(locationDTO.getCountriesDTO().getCountryId());
			country.setCountryName(locationDTO.getCountriesDTO().getCountryName());	
			country.setRegions(region);
			countriesDAO.persist(country);
		}
		
		Locations location = new Locations(locationDTO.getLocationId(), country, locationDTO.getStreetAddress(), locationDTO.getPostalCode(), locationDTO.getCity(), locationDTO.getStateProvince(), null);
		locationsDAO.persist(location);
	}
	
	public void update(Short countryId, LocationsDTO locationDTO) 
	{	
		Locations location = locationsDAO.findById(locationDTO.getLocationId());	
		Countries country = countriesDAO.findById(locationDTO.getCountriesDTO().getCountryId());
		
		location.setStreetAddress(locationDTO.getStreetAddress());
		location.setPostalCode(locationDTO.getPostalCode());
		location.setCity(locationDTO.getCity());
		location.setStateProvince(locationDTO.getStateProvince());
		location.setCountries(country);	
		locationsDAO.update(location);
	}

}
