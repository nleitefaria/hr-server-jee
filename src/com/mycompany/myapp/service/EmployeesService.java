package com.mycompany.myapp.service;

import java.util.ArrayList;
import java.util.List;

import com.mycompany.myapp.dao.EmployeesDAO;
import com.mycompany.myapp.dao.JobsDAO;
import com.mycompany.myapp.domain.EmployeesDTO;
import com.mycompany.myapp.domain.JobsDTO;
import com.mycompany.myapp.entity.Employees;
import com.mycompany.myapp.entity.Jobs;

public class EmployeesService {
	
	private EmployeesDAO employeesDAO;
	private JobsDAO jobsDAO;
	
	public  EmployeesService()
	{
		this.employeesDAO = new EmployeesDAO();
		this.jobsDAO = new JobsDAO();
	}
	
	public EmployeesDTO findById(int id)
	{	
		Employees employees = employeesDAO.findById(id);		
		JobsDTO jobDTO = new JobsDTO(employees.getJobs().getJobId(), employees.getJobs().getJobTitle(), employees.getJobs().getMinSalary(), employees.getJobs().getMaxSalary());		
		EmployeesDTO employeesDTO = new EmployeesDTO(employees.getEmployeeId(), jobDTO, employees.getFirstName(), employees.getLastName(), employees.getEmail(), employees.getPhoneNumber(), employees.getHireDate(), employees.getSalary(), employees.getCommissionPct());
		return employeesDTO;
	}
	
	public List<EmployeesDTO> findAll() 
	{
		List<EmployeesDTO> employeesDTOList = new ArrayList<EmployeesDTO>();		
		List<Employees> employeesList = employeesDAO.findAll();
		
		for(Employees employees : employeesList)
		{
			JobsDTO jobDTO = new JobsDTO(employees.getJobs().getJobId(), employees.getJobs().getJobTitle(), employees.getJobs().getMinSalary(), employees.getJobs().getMaxSalary());		
			EmployeesDTO employeesDTO = new EmployeesDTO(employees.getEmployeeId(), jobDTO, employees.getFirstName(), employees.getLastName(), employees.getEmail(), employees.getPhoneNumber(), employees.getHireDate(), employees.getSalary(), employees.getCommissionPct());
			employeesDTOList.add(employeesDTO);			
		}		
		return employeesDTOList;
	}
	
	public void persist(EmployeesDTO employeeDTO) 
	{			
		Jobs jobs = jobsDAO.findById(employeeDTO.getJobs().getJobId());
		
		if(jobs == null)
		{
			Jobs job = new Jobs();
			job.setJobId(employeeDTO.getJobs().getJobId());
			job.setJobTitle(employeeDTO.getJobs().getJobTitle());
			job.setMinSalary(employeeDTO.getJobs().getMinSalary());
			job.setMaxSalary(employeeDTO.getJobs().getMaxSalary());
			jobsDAO.persist(job);		
		}
		
		Employees employee = new Employees(employeeDTO.getEmployeeId(), jobs, employeeDTO.getFirstName(), employeeDTO.getLastName(), employeeDTO.getEmail(), employeeDTO.getHireDate());
		employeesDAO.persist(employee);
	}
	
	public void update(int emlpoyeeId, EmployeesDTO employeeDTO) 
	{	
		Jobs jobs = jobsDAO.findById(employeeDTO.getJobs().getJobId());
		Employees employee = new Employees(employeeDTO.getEmployeeId(), jobs, employeeDTO.getFirstName(), employeeDTO.getLastName(), employeeDTO.getEmail(), employeeDTO.getHireDate());
		employeesDAO.update(employee);
	}

}
