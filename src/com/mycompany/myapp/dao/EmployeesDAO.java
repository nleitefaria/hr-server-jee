package com.mycompany.myapp.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mycompany.myapp.entity.Employees;
import com.mycompany.myapp.util.HibernateUtil;

public class EmployeesDAO
{
	Session session = null;

	public EmployeesDAO() 
	{
		this.session = HibernateUtil.getSessionFactory().openSession();
	}
	
	@SuppressWarnings("unchecked")
	public List<Employees> findAll() {
		List<Employees> employeesList = (List<Employees>) session.createQuery("from Employees").list();
		return employeesList;
	}
	
	public Employees findById(int id) {
		Employees employee = (Employees) session.get(Employees.class, id);
		return employee; 
	}
	
	
	public void persist(Employees entity) 
	{
		Transaction tx = null;
		try 
		{
		   tx = session.beginTransaction();		   
		   session.save(entity);
		   tx.commit();
		}
		catch (Exception e) 
		{
		   if (tx!=null) 
			   tx.rollback();
		   e.printStackTrace(); 
		}
		finally 
		{
		   session.close();
		}
	}

	public void update(Employees entity) 
	{
		Transaction tx = null;
		try 
		{
		   tx = session.beginTransaction();		  
		   session.update(entity);
		   tx.commit();
		}
		catch (Exception e) 
		{
		   if (tx!=null) 
			   tx.rollback();
		   e.printStackTrace(); 
		}
		finally 
		{
		   session.close();
		}
	}

}
