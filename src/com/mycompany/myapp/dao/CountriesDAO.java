package com.mycompany.myapp.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;

import com.mycompany.myapp.domain.PagginatedDTO;
import com.mycompany.myapp.entity.Countries;
import com.mycompany.myapp.util.HibernateUtil;

public class CountriesDAO 
{   
	Session session = null;

	public CountriesDAO() 
	{
		this.session = HibernateUtil.getSessionFactory().openSession();
	}
	
	public Countries findById(String id) 
	{
		Countries country = (Countries) session.get(Countries.class, id);
		return country; 
	}
	
	@SuppressWarnings("unchecked")
	public List<Countries> findAll()
	{
		List<Countries> countriesList = (List<Countries>) session.createQuery("from Countries").list();
		return countriesList;
	}
	
	@SuppressWarnings("unchecked")
	public PagginatedDTO<Countries> findAllPaginated(int pageNum, int pageSize)
	{	
		Criteria criteria = session.createCriteria(Countries.class);
		criteria.setFirstResult((pageNum - 1) * pageSize);	
		criteria.setMaxResults(pageSize);

		List<Countries> results = criteria.list();
		
		Criteria criteriaCount = session.createCriteria(Countries.class);
		criteriaCount.setProjection(Projections.rowCount());
		Long countResults = (Long) criteriaCount.uniqueResult();
		int lastPageNumber = (int) ((countResults / pageSize) + 1);  
		
	    return new PagginatedDTO<Countries>(results, pageNum, results.size(), lastPageNumber, countResults);
	}
	
	public void persist(Countries entity) 
	{
		Transaction tx = null;
		try 
		{
		   tx = session.beginTransaction();		   
		   session.save(entity);
		   tx.commit();
		}
		catch (Exception e) 
		{
		   if (tx!=null) 
			   tx.rollback();
		   e.printStackTrace(); 
		}
		finally 
		{
		   session.close();
		}
	}

	public void update(Countries entity) 
	{
		Transaction tx = null;
		try 
		{
		   tx = session.beginTransaction();		  
		   session.update(entity);
		   tx.commit();
		}
		catch (Exception e) 
		{
		   if (tx!=null) 
			   tx.rollback();
		   e.printStackTrace(); 
		}
		finally 
		{
		   session.close();
		}
	}
}
