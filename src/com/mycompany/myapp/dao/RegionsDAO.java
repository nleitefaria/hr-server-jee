package com.mycompany.myapp.dao;

import java.math.BigDecimal;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import com.mycompany.myapp.entity.Regions;
import com.mycompany.myapp.util.HibernateUtil;

public class RegionsDAO
{
	Session session = null;

	public RegionsDAO() 
	{
		this.session = HibernateUtil.getSessionFactory().openSession();
	}
	
	@SuppressWarnings("unchecked")
	public List<Regions> findAll() {
		List<Regions> regionsList = (List<Regions>) session.createQuery("from Regions").list();
		return regionsList;
	}
	
	public Regions findById(BigDecimal id) {
		Regions region = (Regions) session.get(Regions.class, id);
		return region; 
	}
	
	public void persist(Regions entity) 
	{
		Transaction tx = null;
		try 
		{
		   tx = session.beginTransaction();		   
		   session.save(entity);
		   tx.commit();
		}
		catch (Exception e) 
		{
		   if (tx!=null) 
			   tx.rollback();
		   e.printStackTrace(); 
		}
		finally 
		{
		   session.close();
		}
	}

	public void update(Regions entity) 
	{
		Transaction tx = null;
		try 
		{
		   tx = session.beginTransaction();		  
		   session.update(entity);
		   tx.commit();
		}
		catch (Exception e) 
		{
		   if (tx!=null) 
			   tx.rollback();
		   e.printStackTrace(); 
		}
		finally 
		{
		   session.close();
		}
	}

}
