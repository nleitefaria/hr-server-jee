package com.mycompany.myapp.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mycompany.myapp.entity.Locations;
import com.mycompany.myapp.util.HibernateUtil;

public class LocationsDAO 
{
	
	Session session = null;

	public LocationsDAO() 
	{
		this.session = HibernateUtil.getSessionFactory().openSession();
	}
	
	@SuppressWarnings("unchecked")
	public List<Locations> findAll() {
		List<Locations> locationsList = (List<Locations>) session.createQuery("from Locations").list();
		return locationsList;
	}
	
	public Locations findById(short id) {
		Locations location = (Locations) session.get(Locations.class, id);
		return location; 
	}
	
	public void persist(Locations entity) 
	{
		Transaction tx = null;
		try 
		{
		   tx = session.beginTransaction();		   
		   session.save(entity);
		   tx.commit();
		}
		catch (Exception e) 
		{
		   if (tx!=null) 
			   tx.rollback();
		   e.printStackTrace(); 
		}
		finally 
		{
		   session.close();
		}
	}

	public void update(Locations entity) 
	{
		Transaction tx = null;
		try 
		{
		   tx = session.beginTransaction();		  
		   session.update(entity);
		   tx.commit();
		}
		catch (Exception e) 
		{
		   if (tx!=null) 
			   tx.rollback();
		   e.printStackTrace(); 
		}
		finally 
		{
		   session.close();
		}
	}
	
	

}
