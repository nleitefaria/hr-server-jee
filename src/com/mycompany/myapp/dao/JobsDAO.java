package com.mycompany.myapp.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;

import com.mycompany.myapp.domain.PagginatedDTO;
import com.mycompany.myapp.entity.Jobs;
import com.mycompany.myapp.util.HibernateUtil;

public class JobsDAO
{
	Session session = null;

	public JobsDAO() 
	{
		this.session = HibernateUtil.getSessionFactory().openSession();
	}
	
	@SuppressWarnings("unchecked")
	public List<Jobs> findAll() {
		List<Jobs> jobsList = (List<Jobs>) session.createQuery("from Jobs").list();
		return jobsList;
	}
	
	public Jobs findById(String id) {
		Jobs job = (Jobs) session.get(Jobs.class, id);
		return job; 
	}
	
	
	@SuppressWarnings("unchecked")
	public PagginatedDTO<Jobs> findAllPaginated(int pageNum, int pageSize)
	{	
		Criteria criteria = session.createCriteria(Jobs.class);
		criteria.setFirstResult(pageNum);
		criteria.setMaxResults(pageSize);
		List<Jobs> results = criteria.list();
		
		Criteria criteriaCount = session.createCriteria(Jobs.class);
		criteriaCount.setProjection(Projections.rowCount());
		Long countResults = (Long) criteriaCount.uniqueResult();
		int lastPageNumber = (int) ((countResults / pageSize) + 1);  
		
	    return new PagginatedDTO<Jobs>(results, pageNum, results.size(), lastPageNumber, countResults);
	}
	
	
	
	public void persist(Jobs entity) 
	{
		Transaction tx = null;
		try 
		{
		   tx = session.beginTransaction();		   
		   session.save(entity);
		   tx.commit();
		}
		catch (Exception e) 
		{
		   if (tx!=null) 
			   tx.rollback();
		   e.printStackTrace(); 
		}
		finally 
		{
		   session.close();
		}
	}

	public void update(Jobs entity) 
	{
		Transaction tx = null;
		try 
		{
		   tx = session.beginTransaction();		  
		   session.update(entity);
		   tx.commit();
		}
		catch (Exception e) 
		{
		   if (tx!=null) 
			   tx.rollback();
		   e.printStackTrace(); 
		}
		finally 
		{
		   session.close();
		}
	}

}
