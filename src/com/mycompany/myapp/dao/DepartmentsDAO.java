package com.mycompany.myapp.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mycompany.myapp.entity.Departments;
import com.mycompany.myapp.util.HibernateUtil;

public class DepartmentsDAO
{
	Session session = null;

	public DepartmentsDAO() 
	{
		this.session = HibernateUtil.getSessionFactory().openSession();
	}
	
	@SuppressWarnings("unchecked")
	public List<Departments> findAll() {
		List<Departments> departmentsList = (List<Departments>) session.createQuery("from Departments").list();
		return departmentsList;
	}
	
	public Departments findById(short id) {
		Departments region = (Departments) session.get(Departments.class, id);
		return region; 
	}
	
	public void persist(Departments entity) 
	{
		Transaction tx = null;
		try 
		{
		   tx = session.beginTransaction();		   
		   session.save(entity);
		   tx.commit();
		}
		catch (Exception e) 
		{
		   if (tx!=null) 
			   tx.rollback();
		   e.printStackTrace(); 
		}
		finally 
		{
		   session.close();
		}
	}

	public void update(Departments entity) 
	{
		Transaction tx = null;
		try 
		{
		   tx = session.beginTransaction();		  
		   session.update(entity);
		   tx.commit();
		}
		catch (Exception e) 
		{
		   if (tx!=null) 
			   tx.rollback();
		   e.printStackTrace(); 
		}
		finally 
		{
		   session.close();
		}
	}
	
	

}
