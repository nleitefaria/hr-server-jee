package com.mycompany.myapp.rws;

import java.math.BigDecimal;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.mycompany.myapp.domain.RegionsDTO;
import com.mycompany.myapp.service.RegionsService;

@Path("/regions")
public class RegionsResource 
{
	private RegionsService regionsService;
	
	public RegionsResource()
	{
		regionsService = new RegionsService();
	}
	
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public List<RegionsDTO> findAll() 
	{
		return regionsService.findAll();
	}
	
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	 public RegionsDTO findById(@PathParam("id") BigDecimal id)
	{
	      return regionsService.findById(id);
	}
	
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response persist(RegionsDTO regionDTO) 
	{
		regionsService.persist(regionDTO);
		return Response.status(201).entity("CREATED DATA").build();		
	}
	
	@PUT
	@Path("{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response persist(@PathParam("id") BigDecimal id, RegionsDTO regionDTO) 
	{
		regionsService.update(id, regionDTO);
		return Response.status(200).entity("UPDATED DATA").build();		
	}

}
