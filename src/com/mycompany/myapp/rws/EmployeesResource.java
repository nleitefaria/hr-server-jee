package com.mycompany.myapp.rws;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.mycompany.myapp.domain.EmployeesDTO;
import com.mycompany.myapp.service.EmployeesService;

@Path("/employees")
public class EmployeesResource {
	
	private EmployeesService employeesService;
	
	public EmployeesResource()
	{
		employeesService = new EmployeesService();
	}
	
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public List<EmployeesDTO> findAll() 
	{
		return employeesService.findAll();
	}
	
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	 public EmployeesDTO findById(@PathParam("id") int id)
	{
	      return employeesService.findById(id);
	}
	
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response persist(EmployeesDTO employeesDTO) 
	{
		employeesService.persist(employeesDTO);
		return Response.status(201).entity("CREATED DATA").build();		
	}
	
	@PUT
	@Path("{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response persist(@PathParam("id") int id, EmployeesDTO employeesDTO) 
	{
		employeesService.update(id, employeesDTO);
		return Response.status(200).entity("UPDATED DATA").build();		
	}

}
