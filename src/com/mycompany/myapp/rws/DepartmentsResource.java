package com.mycompany.myapp.rws;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.mycompany.myapp.domain.DepartmentsDTO;
import com.mycompany.myapp.service.DepartmentsService;

@Path("/departments")
public class DepartmentsResource {
	
	private DepartmentsService departmentsService;
	
	public DepartmentsResource()
	{
		departmentsService = new DepartmentsService();
	}
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public List<DepartmentsDTO> findAll() 
	{
		return departmentsService.findAll();
	}
	
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	 public DepartmentsDTO findById(@PathParam("id") Short id)
	{
	      return departmentsService.findById(id);
	}
	
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response persist(DepartmentsDTO depatmentDTO) 
	{
		departmentsService.persist(depatmentDTO);
		return Response.status(201).entity("CREATED DATA").build();		
	}
	
	@PUT
	@Path("{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response persist(@PathParam("id") short id, DepartmentsDTO depatmentDTO) 
	{
		departmentsService.update(id, depatmentDTO);
		return Response.status(200).entity("UPDATED DATA").build();		
	}

}
