package com.mycompany.myapp.rws;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.mycompany.myapp.domain.CountriesDTO;
import com.mycompany.myapp.domain.PagginatedDTO;
import com.mycompany.myapp.service.CountriesService;

@Path("/countries")
public class CountriesResource 
{
	private CountriesService countriesService;
	
	public CountriesResource()
	{
		countriesService = new CountriesService();
	}
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public List<CountriesDTO> findAll() 
	{
		return countriesService.findAll();
	}
	
	@GET
	@Path("/pagginated")
	@Produces(MediaType.APPLICATION_JSON)
	public PagginatedDTO<CountriesDTO> findAllPaginated(@QueryParam("pageNum") int pageNum, @QueryParam("pageSize") int pageSize) 
	{
		return countriesService.findAllPaginated(pageNum, pageSize);
	}
	
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	 public CountriesDTO findById(@PathParam("id") String id)
	{
	      return countriesService.findById(id);
	}
	
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response persist(CountriesDTO countryDTO) 
	{
		countriesService.persist(countryDTO);
		return Response.status(201).entity("CREATED DATA").build();		
	}
	
	@PUT
	@Path("{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response persist(@PathParam("id") String id, CountriesDTO countryDTO) 
	{
		countriesService.update(id, countryDTO);
		return Response.status(200).entity("UPDATED DATA").build();		
	}
}
