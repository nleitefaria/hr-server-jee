package com.mycompany.myapp.rws;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.mycompany.myapp.domain.JobsDTO;
import com.mycompany.myapp.domain.PagginatedDTO;
import com.mycompany.myapp.service.JobsService;

@Path("/jobs")
public class JobResource {
	
	private JobsService jobsService;
	
	public JobResource()
	{
		jobsService = new JobsService();
	}
		
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public List<JobsDTO> findAll() 
	{
		return jobsService.findAll();
	}
	
	@GET
	@Path("/pagginated")
	@Produces(MediaType.APPLICATION_JSON)
	public PagginatedDTO<JobsDTO> findAllPaginated(@QueryParam("pageNum") int pageNum, @QueryParam("pageSize") int pageSize) 
	{
		return jobsService.findAllPaginated(pageNum, pageSize);
	}
	
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	 public JobsDTO findById(@PathParam("id") String id)
	{
	      return jobsService.findById(id);
	}
	
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response persist(JobsDTO jobDTO) 
	{
		jobsService.persist(jobDTO);
		return Response.status(201).entity("CREATED DATA").build();		
	}
	
	@PUT
	@Path("{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response persist(@PathParam("id") String id, JobsDTO jobDTO) 
	{
		jobsService.update(id, jobDTO);
		return Response.status(200).entity("UPDATED DATA").build();		
	}

}
