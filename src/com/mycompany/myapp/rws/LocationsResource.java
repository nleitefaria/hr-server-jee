package com.mycompany.myapp.rws;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.mycompany.myapp.domain.LocationsDTO;
import com.mycompany.myapp.service.LocationsService;

@Path("/locations")
public class LocationsResource 
{
	private LocationsService locationsService;
	
	public LocationsResource()
	{
		locationsService = new LocationsService();
	}
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public List<LocationsDTO> findAll() 
	{
		return locationsService.findAll();
	}
	
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	 public LocationsDTO findById(@PathParam("id") Short id)
	{
	      return locationsService.findById(id);
	}
	
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response persist(LocationsDTO locationDTO) 
	{
		locationsService.persist(locationDTO);
		return Response.status(201).entity("CREATED DATA").build();		
	}
	
	@PUT
	@Path("{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response persist(@PathParam("id") Short id, LocationsDTO locationDTO) 
	{
		locationsService.update(id,locationDTO);
		return Response.status(200).entity("UPDATED DATA").build();		
	}
	
	

}
