package com.mycompany.myapp.domain;

public class DepartmentsDTO 
{
	
	private short departmentId;   
    private String departmentName;
    private LocationsDTO locationsDTO;
    
	public DepartmentsDTO() 
	{		
	}

	public DepartmentsDTO(short departmentId, String departmentName, LocationsDTO locationsDTO) {
		this.departmentId = departmentId;
		this.departmentName = departmentName;
		this.locationsDTO = locationsDTO;
	}

	public short getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(short departmentId) {
		this.departmentId = departmentId;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public LocationsDTO getLocations() {
		return locationsDTO;
	}

	public void setLocations(LocationsDTO locationsDTO) {
		this.locationsDTO = locationsDTO;
	}
	
	

	
}
