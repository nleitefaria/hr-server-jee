package com.mycompany.myapp.domain;

public class CountriesDTO 
{	
	private String countryId;
	private RegionsDTO regions;
    private String countryName;
       
    public CountriesDTO() {	
	}

	public CountriesDTO(String countryId, RegionsDTO regions, String countryName) {
		this.countryId = countryId;
		this.regions = regions;
		this.countryName = countryName;
		
	}

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public RegionsDTO getRegions() {
		return regions;
	}

	public void setRegions(RegionsDTO regions) {
		this.regions = regions;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	
	
	
    
	
}
