package com.mycompany.myapp.domain;


public class LocationsDTO {
	
	private short locationId;
	private CountriesDTO countriesDTO;
    private String streetAddress;
    private String postalCode;
    private String city;
    private String stateProvince;
    
	public LocationsDTO() {		
	}

	public LocationsDTO(short locationId, CountriesDTO countriesDTO, String streetAddress, String postalCode, String city, String stateProvince) 
	{		
    	this.locationId = locationId;
		this.countriesDTO = countriesDTO;
		this.streetAddress = streetAddress;
		this.postalCode = postalCode;
		this.city = city;
		this.stateProvince = stateProvince;
	}

	public short getLocationId() {
		return locationId;
	}

	public void setLocationId(short locationId) {
		this.locationId = locationId;
	}

	public CountriesDTO getCountriesDTO() {
		return countriesDTO;
	}

	public void setCountriesDTO(CountriesDTO countriesDTO) {
		this.countriesDTO = countriesDTO;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStateProvince() {
		return stateProvince;
	}

	public void setStateProvince(String stateProvince) {
		this.stateProvince = stateProvince;
	}
	
	

	
	

	

}
