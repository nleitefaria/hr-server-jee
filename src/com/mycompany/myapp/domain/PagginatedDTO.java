package com.mycompany.myapp.domain;

import java.util.List;

public class PagginatedDTO<T> {
	
	private List<T> results;
	private int number;
	private int numberOfElements;	
	private int totalPages;
	private long totalElements;
	
	public PagginatedDTO()
	{
	}

	public PagginatedDTO(List<T> results, int number, int numberOfElements, int totalPages, long totalElements) {
		this.results = results;		
		this.number = number;
		this.numberOfElements = numberOfElements;
		this.totalPages = totalPages;
		this.totalElements = totalElements;
	}
	
	public List<T> getResults() {
		return results;
	}
	public void setResults(List<T> results) {
		this.results = results;
	}
	
	
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public int getNumberOfElements() {
		return numberOfElements;
	}
	public void setNumberOfElements(int numberOfElements) {
		this.numberOfElements = numberOfElements;
	}
	public int getTotalPages() {
		return totalPages;
	}
	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public long getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(long totalElements) {
		this.totalElements = totalElements;
	}
	
	
	
	

}
