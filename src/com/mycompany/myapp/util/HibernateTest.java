package com.mycompany.myapp.util;

import java.math.BigDecimal;

import com.mycompany.myapp.domain.CountriesDTO;
import com.mycompany.myapp.domain.DepartmentsDTO;
import com.mycompany.myapp.domain.LocationsDTO;
import com.mycompany.myapp.domain.RegionsDTO;
import com.mycompany.myapp.service.DepartmentsService;
import com.mycompany.myapp.service.LocationsService;

public class HibernateTest {
	
	public static void main(String[] args) {
        
        
        
        test2();
 
    }
	
	static void test1()
	{
		DepartmentsService dService = new DepartmentsService();
        
        LocationsDTO locationsDTO = new LocationsDTO();
        locationsDTO.setLocationId((short)2700);
              
        DepartmentsDTO dDTO = new DepartmentsDTO();
        dDTO.setDepartmentId((short)29);
        dDTO.setDepartmentName("yyy");
        dDTO.setLocations(locationsDTO);
        dService.persist(dDTO);	
	}
	
	static void test2()
	{
		LocationsService lService = new LocationsService();
        
		RegionsDTO regionDTO = new RegionsDTO();
		regionDTO.setRegionId(new BigDecimal(1));
        
        CountriesDTO cDTO = new CountriesDTO();
        cDTO.setCountryId("PT");
        cDTO.setCountryName("Portugal");
        cDTO.setRegions(regionDTO);
        
        LocationsDTO locationsDTO = new LocationsDTO((short)1003, cDTO, "RUA YPTO", "2222", "Lisbon", "Lisboa");
        
        
        
        lService.persist(locationsDTO );	
	}

}
